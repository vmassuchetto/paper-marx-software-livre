\documentclass[article,12pt,oneside,a4paper,english]{abntex2}

\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{indentfirst}
\usepackage{nomencl}
\usepackage{color}
\usepackage{graphicx}
\usepackage[alf]{abntex2cite}
\usepackage[none]{hyphenat}
\usepackage{footnote}

\def\citeyearpar#1{(\citeyear{#1})}
\makesavenoteenv{tabular}

\titulo{Trabalho colaborativo na sociedade da informação: representações da
  teoria marxista no software livre}
\autor{Vinicius Massuchetto\thanks{Tecnólogo em Química Ambiental pela Universidade
  Tecnológica Federal do Paraná, Bacharel em Ciência da Computação pela
  Universidade Federal do Paraná, Mestrando no Programa de Pós-Graduação em
  Tecnologia Nuclear do Instituto de Pesquisas Energéticas e Nucleares da
  Universidade de São Paulo.}\thanks{E-mail de contato:
  vmassuchetto@gmail.com}}
\local{São Paulo}
\data{2013}

\interfootnotelinepenalty=10000

\makeatletter
\hypersetup{
  pdftitle={\@title},
  pdfauthor={\@author},
  pdfsubject={Modelo de artigo científico com abnTeX2},
  pdfcreator={LaTeX with abnTeX2},
  pdfkeywords={abnt}{latex}{abntex}{abntex2}{atigo científico},
  colorlinks=false,
  pdfborder={0 0 0},
  bookmarksdepth=4}

\makeatother
\makeindex

\setlrmarginsandblock{4cm}{4cm}{*}
\setulmarginsandblock{4cm}{4cm}{*}
\checkandfixthelayout
\setlength{\parindent}{1.3cm}
\setlength{\parskip}{0.2cm}
\SingleSpacing

\begin{document}

\sloppy
\frenchspacing
\maketitle

\begin{resumoumacoluna}

  \noindent
  Resumo

  \vspace{0.1cm}

  \noindent
  \textbf{Palavras-chaves}: colaboratividade, software livre, sociedade da
  informação

\end{resumoumacoluna}

\section*{Introdução}

Este trabalho propõe-se a tratar de um tema cruzado sobre duas vertentes que,
embora tenham alguma relação em sua natureza, não comumente se relacionam
quando são tratadas em seus devidos espaços de discussão. O software livre há
muito tem se afirmado como um ferramental alternativo à dominância dos mercados
de software, e mais recentemente tem inspirado discussões políticas sobre a
soberania dos governos em meio a fatores decisivos que envolvem a tecnologia em
nossa sociedade atual, já que sua adoção em políticas públicas e outros âmbitos
da sociedade não implicam em uma série de questões trazidas pelos softwares
proprietários e segredos de código. Esta aproximação política e do debate
ideológico que tomam as liberdades como uma premissa às ações nos levam a
observar outros conceitos já estabelecidos pela teoria marxista, e que
questiona vem a questionar propostas de liberdade nos mais variados espaços
sociais.

Ainda que de maneira informal, o software livre surge em meio à este contexto
para atender inicialmente as demandas acadêmicas, e foi aos poucos
integrando-se ao mercado e hoje mostra-se como uma alternativa viável às mais
diversas aplicações. Através de um engenhoso sistema de subversão dos
mecanismos regulatórios, o software livre possui a garantia de distribuição das
informações que o compõem através de um modelo livre de colaboração que
conhecemos por \emph{copyleft}, sendo assim capaz de modificar algumas relações
econômicas na área da tecnologia.

Além de conceituar sua temática, o trabalho busca subsidiar a escolha dos
parâmetros marxistas utilizados para analisar a configuração deste modelo de
produção, e apoia-se em autores que trouxeram a discussão marxista para além da
consolidação imperialista das décadas de 80 e 90, e que procuraram descrever os
processos de acumulação capitalista de uma sociedade que pauta fortemente a sua
economia no campo informacional.

\section*{Software livre}

Dentre as subdivisões dos componentes de um computador, o termo software é
constantemente dissociado em conceitos menores e que são usados pelos autores
para atender seus respectivos contextos. Uma definição convencional para nós é
dada por \cite{Engelhardt2008}, em que software são ``todos os
componentes funcionais não físicos de um computador, e portanto, não somente os
programas em si, mas também os dados a serem processados por eles''.

Um programa de software entendido pelos componentes físicos e eletrônicos de um
computador nada mais são do que instruções de máquina codificadas em um formato
binário -- constituída somente e simbolicamente pelos algarismos 0 e 1. A
geração desta codificação é feita a partir de um outro código escrito em uma
linguagem de programação , o que por sua vez é chamado de código fonte do
programa. A detenção do código fonte determina a possibilidade de um
profissional de software em modificar as instruções de um programa e assim
definir as suas regras e estruturas de funcionamento.

O entendimento do que é um software livre dá-se em torno da disponibilidade
deste código fonte, o que permite que não somente uma pessoa ou organização
modifique e distribua um programa sendo respaldado legalmente e tecnicamente,
mas sim qualquer pessoa que possua o conhecimento necessário para tal. A
definição clássica dada por \cite{Stallman2007} permite caracterizar um
software como sendo livre quando ele possui as seguintes liberdades:

\begin{itemize}

  \item{Liberdade 0: A liberdade de executar um programa para qualquer que seja
  a finalidade e em qualquer condição;}

  \item{Liberdade 1: A liberdade de estudar um programa e de modificá-lo como
  for desejado -- ter acesso ao código-fonte é uma condição para esta
  liberdade;}

  \item{Liberdade 2: A liberdade de redistribuir cópias de um programa, e assim
  ajudar outras pessoas a ter acesso a este programa;}

  \item{Liberdade 3: A liberdade de melhorar um programa e de distribuir
  novas versões para o público, e assim beneficiar toda a comunidade.}

\end{itemize}

O respaldo regulatório ao qual os softwares livres se apoiam para garantir que
sua distribuição implique nestas quatro liberdades são as licenças livres -- ou
licenças \emph{copyleft}.  Estas licenças se utilizam de uma subversão do
sistema de \emph{copyright}, em que o autor ao invés de reservar todos os
direitos sobre um software, assegura através de termos jurídicos que o software
está na verdade disponível para qualquer pessoa, e que ninguém poderá
distribuir trabalhos derivados dele se não o fizer sob a mesma licença. Dentre
as mais comuns estão as licenças GPL, BSD e MIT.

Também é necessário fazer a distinção entre liberdade e gratuidade -- não
somente porque a palavra \emph{free} do inglês possui ambos os significados,
mas também porque o conceito é frequentemente confundido. Embora a maior parte
dos softwares livres sejam gratuitos, temos que boa parte dos softwares
gratuitos não são livres segundo as quatro liberdades enunciadas acima. Nestes
softwares -- os chamados \emph{freewares}, existe uma liberdade muitas vezes
restrita para executar o programa, mas não podemos ver seu código fonte e muito
menos modificar e redistribuir cópias personalizadas dele.

A preferência pelo software livre, suas vantagens e desvantagens são um assunto
bastante discutido tanto do ponto de vista técnico, econômico quanto
ideológico. Para o mercado de software de uma maneira mais específica,
\cite{Kaminsky2009} lista algumas das principais vantagens do software
livre sobre o proprietário:

\begin{itemize}

  \item{Uma maior participação de desenvolvedores pelo mundo todo, o que
  resulta em um software mais revisado e seguro, já que mais pessoas têm acesso
  ao código-fonte e são capazes de identificar suas vulnerabilidades;}

  \item{O aumento da dificuldade para formação de monopólios de prestação de
  serviços, pois as soluções estão disponíveis também para outras empresas que
  podem a qualquer momento obter uma cópia do produto em questão e prestar os
  mesmos serviços em relação a ele;}

  \item{É possível que os recrutadores tenham um melhor conhecimento das
  habilidades dos desenvolvedores ao selecioná-los para um determinado cargo,
  já é possível também ter acesso às contribuições dadas por eles aos projetos
  de software livre.}

\end{itemize}

Para este trabalho, no entanto, o valor fornecido pelo software livre a ser
mais utilizado é elementar, e tem a ver com a ideia de liberdade tão presente
na atividade de desenvolvimento de software. Trata-se da propriedade
intelectual, ou ainda de forma mais detalhada, dos direitos sobre os
diferentes tipos de usufruto das invenções em todos os domínios da atividade
humana.

\section*{Sociedade da informação}

Buscando descrever as peculiaridades com que a informação comporta-se nas
interações sociais, alguns autores introduziram desde a década de 90 o conceito
de ciberespaço na teoria da comunicação. \cite{Stallabras1996} afirma que
este conceito possui origem na ficção científica das fantásticas novelas que se
passam em mundos e circunstâncias futurísticas -- literatura que ficou famosa a
partir da década de 30 e que mais tarde deu origem à cultura \emph{cyberpunk}.
Uma boa descrição foi dada por \cite{Levy1994} através de um paralelo
desta realidade com o meio físico e material:

\begin{citacao}

  Assim como se diz ``tem areia'', ``tem água'' se diz ``tem textos'', ``tem
  mensagens'' pois eles se tornam matérias como se fossem fluxos justamente
  porque o suporte deles não é fixo, porque no seio do espaço cibernético
  qualquer elemento tem a possibilidade de interação com qualquer outro
  elemento presente. Então, isso não é uma utopia daqueles que experimentaram,
  conhecem e participam da Internet. É como se todos os textos fizessem parte
  de um texto, só que é o hipertexto, um autor coletivo e que está em
  transformação permanente. É como se todas as músicas passassem a fazer parte
  de uma mesma polifonia virtual e potencial, como se todas as músicas fizessem
  parte de uma só música, também ela virtual e potencial. \cite[p.
  3]{Levy1994}.

\end{citacao}

Embora não seja consensual entre os estudiosos da área de que já ultrapassamos
um marco que torna possível a definição de um novo modelo de sociedade, este
trabalho adota o termo `sociedade da informação' para caracterizar o fenômeno
recente de inserção da tecnologia na sociedade. Segundo
\cite{Castells1999}, as diversas revoluções tecnológicas ocorridas ao
longo da história são marcadas, basicamente, por sua penetrabilidade em todos
os domínios da atividade humana. Em outras palavras, define-se uma nova era
quando as inovações tecnológicas induzem uma profunda modificação nas relações
entre as pessoas, o que envolve, em nosso principal interesse, as relações de
poder e produção em um sistema econômico. Para \cite{Masuda1980}, a
magnitude destas transformações é equiparável à outros marcos singulares da
história.

\begin{citacao}

  Na história documentada, existem três impulsos de mudança fortes o suficiente
  para alterar o homem em sua essência. A introdução da agricultura [\ldots] a
  revolução industrial [\ldots] [e] a revolução tecnológica de processamento da
  informação [ou revolução da informação].  \cite[grifo nosso]{Masuda1980}.

\end{citacao}

Assim, o uso do termo é colocado de modo a remeter ao novo paradigma de
sociedade em que atualmente nos encontramos, no qual a dinâmica dada à
informação e seus sistemas não define somente o modo de funcionamento dos
mercados, mas também orienta transformações sociais, econômicas e culturais.
Para a interpretação deste fenômeno compartilhamos das percepções de
\cite{Castells1999} e \cite{Soderberg2002}, em que esta revolução
tecnológica não é uma simples evolução dos processos de produção e que visam
meramente a melhoria da qualidade de vida das pessoas, mas sim de uma extensão
das motivações das revoluções anteriores: a de que sociedade do informação nada
mais é do que uma reestruturação do sistema capitalista e que teve por
finalidade principal a ampliação do poderio econômico dos proprietários dos
meios de produção.

Conforme observa \cite{DyerWitheford1999}, nos princípios do delineamento
teórico sobre o uso da tecnologia avançada podemos citar os trabalhos de
Charles Babbage\footnote{Contemporâneo de Marx e considerado o pai da
computação. Inventor da máquina analítica, um sistema mecânico que mais tarde
viria a basear a construção dos primeiros computadores.} sobre economia
política, e que introduz o chamado `gerenciamento científico' dos processos
industriais, cujo principal objetivo era a produção por meios exclusivamente
mecânicos. Ao ter contato com estes trabalhos, \cite{Marx1939} não
enxerga ali o simples caráter tecnocrático do capitalismo, mas também um estudo
estratégico para a lutas de classes. Uma crítica a respeito dos discursos
vangloriantes da tecnologia é colocada nas anotações finais de O Capital,
mostrando de maneira bem clara a sua visão sobre o progresso da humanidade nos
diferentes campos do conhecimento:

\begin{citacao}

  `O progresso contínuo de sabedoria e experiência', diz Babbage, `é o nosso
  grande poder'. Esta progressão, este progresso social pertence e é explorado
  pelo capital. Todas as formas anteriores de propriedade condenam grande parte
  da humanidade, os escravos, a serem puros instrumentos de trabalho. O
  desenvolvimento histórico, político, artístico, científico, etc. acontece em
  privilegiados círculos sobre suas cabeças. Mas somente o capital subjugou o
  progresso histórico em detrimento do seu enriquecimento. \cite[tradução
  nossa]{Marx1939}.

\end{citacao}

\section*{A sociedade da informação através do marxismo}

Muito discute-se sobre a crise da teoria marxista, não sendo incomum encontrar
declarações sobre sua falência para com as dinâmicas econômicas atuais. Através
do fato de que diversos governos que atribuíram os nomes ``marxismo'' e
``comunismo'' às suas políticas mostraram de diferentes formas e por diferentes
razões -- assim como os governos capitalistas -- uma profunda incompatibilidade
à manutenção de uma ordem social próspera, \cite{Giannotti2011} discute a
validade do marxismo para estudos de contextos atuais:

\begin{citacao}

  O colapso [dos países socialistas] evidenciaram que a luta contra as
  misérias, instaladas pelo sistema capitalista, não implica qualquer
  compromisso com partidos comunistas de cunho leninista. Esse colapso reduz a
  pó a vulgata marxista, mas não impede que se continue a estudar as
  representações e as relações sociais da ótica do metabolismo que o homem
  mantém com a natureza, em suma, daquela que vê as relações sociais de
  produção imbricadas com o desenvolvimento das forças produtivas. [\ldots] a
  obra escrita ilumina-se a partir de certas perspectivas históricas, de certos
  vieses que alimentam modos de pensar e de ver, inscritos em nosso cotidiano.
  \cite[p. 17, grifo nosso]{Giannotti2011}.

\end{citacao}

Assim como \cite{Meszaros2010} faz uma discussão semelhante sobre a
adaptabilidade da teoria base para com novos conceitos:

\begin{citacao}

  [\ldots] a transformação social prevista pela visão marxista deve ser capaz
  de avaliar as dificuldades inerentes à própria magnitude das tarefas a serem
  realizadas, como também enfrentar as contingências sócio-históricas mutáveis
  e inevitáveis, reexaminando as proposições básicas da teoria original e, se
  necessário, adaptando às novas circunstâncias. \cite[grifo
  nosso]{Meszaros2010}.

\end{citacao}

\cite{Soderberg2002} diz que a principal suspeita a respeito da
aplicabilidade de seu método na atualidade dá-se pela percepção do imaginário
coletivo de que os sistemas de informação aos poucos substituirão os
trabalhadores, e que tão logo não haverá campo para aplicação de uma análise
marxista. Uma interpretação desta natureza peca em essência por deixar de
analisar um pouco mais profundamente sobre o que é de fato a informação, e
quais suas demandas de poder, produção e consumo em relação à sociedade.

Uma crítica irônica é feita por \cite{DyerWitheford1999} em relação à
este tipo de posicionamento. O autor dedica todo um capítulo para responder
argumentos que negam a compatibilidade da teoria marxista com a sociedade
contemporânea:

\begin{citacao}

  [\ldots] se o marxismo é tido como obsoleto pela era da informação, é somente
  pela luz de um certo desenvolvimento `informacional' -- globalização, pré
  eminência da mídia, tele-trabalho -- é que podemos ver a completa importância
  de alguns temas presentes nos textos de Marx -- por exemplo, a ênfase [dos
  que pregam o fim do marxismo] na internacionalização e automação da produção.
  [\ldots] o marxismo sempre manifestará uma contínua ``espectrabilidade'', uma
  estranha negação em morrer e ser enterrado, e que está profundamente
  conectada à natureza ``espectral'' e ``imaterial'' do tecnocapitalismo
  contemporâneo. \cite[p. 8, tradução e grifo nosso]{DyerWitheford1999}.

\end{citacao}

\cite{Soderberg2002}, por sua vez, é ainda mais incisivo na temática do
software livre, sendo um dos primeiros autores a produzir conteúdo dedicado e
significativo na relação da liberdade de software com o marxismo:

\begin{citacao}

  O marxismo oferece um bom modelo teórico para análise das contradições
  inerentes do regime de propriedade intelectual. O sucesso do software livre
  em trabalhar fora do sistema comercial de software é uma amostra do que foi
  descrito por Marx há mais de 150 anos sob as formalizações de força produtiva
  e de intelecto geral. [\ldots] a história não se resume ao levante das forças
  produtivas que foram convenientemente mapeadas pelos exemplos do materialismo
  histórico, mas são conflitos protagonizados por atores sociais, dentre eles o
  movimento do software livre e sua característica especial de desafiar a
  dominação do capital sobre o desenvolvimento tecnológico. \cite[tradução e
  grifo nosso]{Soderberg2002}.

\end{citacao}

Este estudo admite que não só pela validade da elucidação teórica se justifica
uma análise marxista da sociedade da informação. Embora o sistema capitalista
tenha se reformulado, se globalizado e adquirido novas dinâmicas de exercício
de poder devido ao avanço tecnológico e à sociedade da informação,
compartilhamos com \cite{Meksenas2008} a percepção de que temos os mesmos
elementos conceituais mantendo os mesmos padrões: o trabalhador explorado, a
forte orientação à propriedade, a centralização do lucro privado, a mais-valia
e a grande desigualdade nas relações de produção.

\section*{O marxismo na era da informação e o discurso autonomista}

Segundo \cite{DyerWitheford1999}, após a estabilização econômica das
grandes potências a partir da crise do petróleo na década de 70, pareceu
existir uma dificuldade do encontro de evidências de que haveria mais cedo ou
mais tarde uma completa derrocada do capitalismo. \cite{Dupas2001}, por
exemplo, discorre em tom incerto por todo um capítulo a respeito da tecnologia
da informação e a hegemonia dos Estados Unidos, sem ainda que houvessem
indicativos claros de uma grave crise econômica naquele país:

\begin{citacao}

  Estamos diante do mais longo ciclo de crescimento econômico dos Estados
  Unidos, [\ldots] a questão sobre quando virá o declínio persegue o mundo todo
  e exige novas explicações. [\ldots] Ainda que várias opiniões apontem para um
  ajuste futuro por conta dos desequilíbrios da chamada ``nova economia'', a
  consolidação da hegemonia é tão impressionante que permite a metáfora de um
  enorme e competente polvo, com seus tentáculos fortemente agarrados na
  tecnologia da informação, a alimentar-se dos mercados globais. \cite[p. 45,
  grifo nosso]{Dupas2001}.

\end{citacao}

Um dos indicadores deste contexto de questionamento foi a circunstância de
eclosão da forte crise de 2007 nos Estados Unidos, que novamente evidenciou a
visão marxista sobre a economia e impulsionou o debate e a atenção para o tema.
A ilustração do artista plástico Azlan McLennan presente na figura
\ref{Fig:MarxRight} é uma resposta da Internet à influência de produção de
material acadêmico e artístico sobre a questão da fragilidade da dominância
tecnológica estadunidense frente à teoria marxista.

\begin{figure}
  \label{Fig:MarxRight}
  \centering
  \includegraphics[width=0.7\textwidth]{./img/marx-right.jpg}
  \caption{Marx na Internet: ``Eu avisei que estava certo sobre o
  capitalismo''}
  Fonte: \cite{McLennan2009}
\end{figure}

As correntes de estudos que se formaram desde a década de 60 construíram
divergências significativas entre si, questionando-se entre si a respeito do
distanciamento dos aspectos ortodoxos da teoria para a construção de novos
conceitos. Dentre toda essa discussão desenvolveu-se o intenso discurso
autonomista, uma linhagem de pensamento sobre o homem remete à sua autonomia
enquanto ser e agir, e que veio em resposta às novas luta cujos integrantes não
possuírem raízes ou vivência operária. \cite{Gurgel2010} coloca que estas
discussões tratam de uma busca pelo sujeito revolucionário de nosso tempo
através de uma análise dentre os movimentos que demonstram uma profunda
insatisfação com a atual estrutura da sociedade, mas que também muitas vezes
não estão inseridos no sistema produtivo sob a plena condição de explorados.

\section*{Representações da teoria marxista no software livre}

Ao analisar o contexto de propriedade no ciberespaço,
\cite{Stallabras1996} apresenta um exercício teórico para definição do
hegelianismo tecnológico, que procura definir o ciberespaço como uma expressão
hegeliana do homem enquanto promotor da tecnologia. Neste raciocínio, se para
Hegel o real é o ideal, no ciberespaço temos que o real é reproduzido de forma
virtualmente ideal, uma vez que as realidades são tecnicamente viabilizadas
através de diferentes implementações computacionais. Este viés teórico define o
ciberespaço mais como um foro único da própria consciência humana do que uma
simples interface de mentes entre o material e o abstrato.

\cite{Marx1939} discute como a revolução dos modos de produção industrial
e agrícola forçaram uma outra revolução paralela nos processos sociais de
produção, tal como foi a forte reestruturação das dinâmicas de comunicação e
transporte. Esta é a introdução para o que ele chama de ``aniquilação do espaço
pelo tempo'', e que segundo \cite{Harvey2010} remete à natureza pouco
neutra do capitalismo em relação às questões geográficas, e que acaba sendo um
princípio bastante promissor para o entendimento de formação da `sociedade da
informação' e sua base imaterial.

\begin{citacao}

  O mais avançado desenvolvimento do capital -- ou quando o capital faz-se
  assumir o modo de produção correspondente -- ocorre não somente quando as
  relações de trabalho tomam a forma econômica de capital fixo, mas quando
  também são suspensas em sua forma imediata, e o capital fixo aparece como uma
  máquina no processo de produção [\ldots], que por sua vez não parece ser
  submedido pelas habilidades diretas de um trabalhador, mas sim como uma
  aplicação tecnológica da ciência. \cite[grifo nosso]{Marx1939}.

\end{citacao}

A capacidade do capitalismo em criar processos produtivos baseados em
informação pode ser interpretado como uma expressão de sua excelência
científica, e que é passível de geração de uma altíssima interdependência entre
os diferentes setores produtivos. No estudo desta característica, conclusões
especialmente notórias são deixadas em um longo parágrafo das páginas finais
dos \emph{Grundrisse}, e mostram a perspicácia de Marx com relação à
compreensão das dinâmicas econômicas e produtivas do capitalismo. O `autômato'
projetado para agir propositadamente é descrito em termos da apropriação de
conhecimento:

\begin{citacao}

  A acumulação de conhecimento e habilidades das forças produtivas gerais do
  cérebro social, é assim absorvida pelo capital como uma oposição ao trabalho,
  e portanto aparece como um atributo do capital, e mais especificadamente, do
  capital fixo, à medida que adentra o processo de produção na própria forma de
  meio de produção. \cite{Marx1939}.

\end{citacao}

\cite{DyerWitheford1999} identifica na obra de \cite{Marx1939} a
previsão de que o capitalismo se subverteria através de sua própria evolução
tecnológica. A excelência tecnocientífica geraria uma tamanha interdependência
das formas de cooperação para produção que tal configuração exacerbaria os
parâmetros até então conhecidos sobre a propriedade.

É aí que chegamos na discussão sobre o caráter imaterial da informação, e que
faz com que para ser capaz de expressar-se no ciberespaço, o capitalismo deva
inferir no controle de liberdades dos indivíduos e assim direcionar a força
produtiva deste meio de acordo com os seus objetivos.  \cite{Lessig2006}
sintetiza quatro restrições de controle neste sentido:

\begin{itemize}

  \item{Leis: regulam o comportamento no ciberespaço através de recursos como
  \emph{copyright}, leis de difamação e classificação etária.}

  \item{Normas sociais: assim como as leis, regulam os diferentes ambientes
  virtuais -- tais como os fóruns, as listas de discussão e redes sociais,
  através do que é socialmente aceito pela comunidade que os formam; contam aí
  os aspectos culturais e os valores morais -- ou moralistas -- dos grupos
  sociais}

  \item{Mercado: basicamente a estrutura de preços para utilização da
  informação, tal como os recursos \emph{paywall} para acesso à notícias em
  jornais, compra de filmes, músicas e livros; admitem também as remunerações
  de publicidade e demais serviços}

  \item{Arquitetura: código; o hardware e o software que constituem o
  ciberespaço e que definem o nível de acesso dos indivíduos às informações,
  tal como barreiras de login e criptografia}

\end{itemize}

A ideia de arquitetura colocada por \cite{Lessig2006} tem um papel
central na lógica da propriedade intelectual do ciberespaço. Embora a cultura
`cracker' tenha especializado-se muito bem em desenvolver softwares de
engenharia reversa para transpor este modo de controle, a crescente
interdependência dos sistemas de informação dificultam cada vez mais o emprego
de técnicas de quebra de segurança.

A ideia do software livre, por outro lado, é moldar o controle jurídico a seu
favor. A partir do momento que as licenças de software se utilizam do modelo
\emph{copyright} para constituição do \emph{copyleft}, faz-se uma eficiente
subversão deste mecanismo para mudar o foco do indivíduo para o coletivo,
alterando os diferentes níveis de privilégios acerca de um software, e
colocando-os à disposição da sociedade como um todo -- constituindo portanto,
um modelo antagônico ao hegemônico, que inicialmente estava plenamente focado
em processos de acumulação capitalista.

No campo do trabalho, \cite{Marx1939} coloca dois aspectos tecnológicos
que seriam indicadores de vigor deste novo modelo tecnocrático do capitalismo:

\begin{itemize}

  \item{Que a crescente automação industrial viria a transformar
  significativamente o trabalho no chão de fábrica, e passariam a ser
  necessários somente trabalhadores com funções indiretas, porém cruciais, para
  gerenciamento das fábricas, divididos em ``trabalhadores científicos'' e
  outras ``combinações sociais''.}

  \item{Que ocorreria a consolidação mundial das economias para a formação de
  um mercado globalizado -- o chamado ``trabalho universal''.}

\end{itemize}

A partir destes dois pontos nos aproximamos da tese autonomista de
\cite{Negri1991} pelo entendimento de que a produtividade em rede na
sociedade da informação depende de uma complexa divisão do trabalho através de
uma rede extremamente estruturada de informação, e que é capaz de abstrair e
digitalizar operações automatizadas, que tornam-se atividade intelectual
produtiva por si só. A visão autonomista nos é útil para abordar o modelo de
trabalho das comunidades de software livre analisando tanto as peculiaridades
do perfil de trabalho quanto a união autônoma em unidades produtivas.

\cite{Negri1991} afirma que o conceito de autovalorização está implícito
nos conceitos desenvolvidos por Marx em relação à classe trabalhadora e seu
programa revolucionário. A autovalorização do trabalhador seria um
condicionante à emergência de um poder autônomo solidificado na maturidade
organizacional, e seria o aspecto viabilizador para a instauração de uma
revolução do proletariado.

Esta afirmação com vista ao software livre precisa ser analisada com bastante
cautela, uma vez que muitas implementações livres servem claramente a grandes
atores do sistema capitalista informacional. Neste sentido, o software livre
não é uma anteposição autêntica e inerentemente anticapitalista, mas sim um
mercado alternativo e que também está voltado para a acumulação. A visão de
\cite{Gurgel2010} neste caso deve ser considerada:

\begin{citacao}

  [A questão da visão autonomista é] problemática para nós, tendo em vista que
  a invenção intelectual ainda se apresenta, no capitalismo, como
  máquina-ferramenta voltada para a demanda do capitalista. Demanda esta por
  mecanismos de aumento de acumulação de seu capital. Em outras palavras, a
  técnica criada, a partir de um engenheiro de computação, por exemplo,
  constitui-se em meios de produção necessários para o ciclo e processo de
  reprodução do capitalismo. A invenção, portanto, se apresenta neste contexto
  como mais uma mercadoria e serve como motor para a criação de novos mercados,
  na linha da flexibilização da produção, via \emph{palavras-açúcar} como
  qualidade total. \cite[p. 35, grifo nosso]{Gurgel2010}.

\end{citacao}

Entendemos que esta visão de \cite{Gurgel2010} não pode ser entendida de
maneira absoluta, uma vez que muitos softwares livres têm também uma posição
política e finalidade comunitária bem definida. \cite{Negri1991} cita,
por exemplo, que nos projetos de autovalorização, além de encontrarmos a
negação ou o poder de destruir a determinação do capital, temos também uma
recomposição da classe trabalhadora através da afirmação criativa e do poder de
constituição de novas práticas. Em outros casos, estes projetos nascem de
dentro do próprio capital através da modificação essencial de elementos
específicos que em algum momento já pertenceram integralmente ao conjunto de
práticas para a acumulação capitalista, sendo portanto, uma espécie de
conversão dos processos de desvalorização.

A tese da auto-valorição descrita por \cite{Negri1991} prevê também que
os projetos contra-hegemônicos -- como sugere-se que o software livre o é --
não se qualificam necessariamente como um projeto social unificado e
revolucionário, mas somente denotam uma pluralidade de instâncias e
possibilidades que surgem através da insatisfação com a configuração do sistema
atual. Assim, estes projetos autônomos podem não só ser caracterizados como um
combate ao capitalismo, mas podem também ser parte dele em uma forma diferente
e alternativa de geração de valor. De acordo com
\cite{DyerWitheford1999}, embora a visão autonomista possa ser
caracterizada pelo abandono de alguns fortes conceitos da estrutura do
raciocínio marxista, ela acaba construindo subsídios interessantes para
analisar a estrutura da comunidade do software livre.

\cite{Soderberg2002} observa que neste sistema a explícita dependência da
economia da informação sobre a capacidade comunicativa e cooperativa da força
de trabalho retira parte do caráter controlador da produção dos agentes
capitalistas, e que um dos indicativos desta perda de controle são as
implementações livres de software e de protocolos de comunicação. Esta
interpretação é delicada quando observamos o ritmo de crescimento cada vez
maior das mega corporações do Vale do Silício às custas do software livre, mas
faz sentido quando vemos uma enorme rede de colaboração possibilitar o acesso à
tecnologia por setores que não o teriam se dependessem dos softwares
proprietários.

\section*{Conclusão}

Compreende-se as contradições a que estamos sujeitos quando desenvolvemos uma
análise desta natureza, mas entende-se também que as dinâmicas informacionais
precisam ser levadas em conta para que possamos abranger as relações sociais da
atualidade, e neste campo as perspectivas marxistas mostram-se como ferramentas
razoáveis para o esclarecimento do que se entende pela liberdade de
conhecimento, e quais as mudanças possíveis na sociedade a partir da
implementação de políticas que valorizem iniciativas relacionadas à cultura da
livre informação.

A tentativa de buscar traços marxistas na totalidade da tecnologia da
informação não exclui nenhuma outra condição presente nas demais complexidades
do sistema capitalista. Para que um computador de última geração possa executar
um aplicativo segundo todas as premissas da liberdade de conhecimento, é certo
que diversas práticas capitalistas clássicas tenham sido empregadas durante a
fabricação deste equipamento. O materialismo histórico percorre inúmeros
escopos e são de incrível diversidade, e acaba revelando no software livre
somente uma parcela de um senso de igualdade social.

Embora o software livre não garanta uma economia da informação igualitária que
pluralize o acesso à tecnologia e que ofereça a todos as mesmas condições,
acredita-se que uma sociedade que verdadeiramente incorpore estas
características não somente utilizará códigos livres com exclusividade, mas
também não verá sentido na propriedade do conhecimento em toda a extensão da
atividade humana.

\pagebreak

\bibliography{mfs.bib}

\end{document}
