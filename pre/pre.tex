\documentclass[article,11pt,oneside,a4paper,english]{abntex2}

\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{indentfirst}
\usepackage{nomencl}
\usepackage{color}
\usepackage{graphicx}
\usepackage[alf]{abntex2cite}
\usepackage[none]{hyphenat}
\usepackage{footnote}

\def\citeyearpar#1{(\citeyear{#1})}
\makesavenoteenv{tabular}

\titulo{Trabalho colaborativo na sociedade da informação: representações da
  teoria marxista no software livre}
\autor{Vinicius Massuchetto}
\local{São Paulo}
\data{2013}

\interfootnotelinepenalty=10000

\makeatletter
\hypersetup{
  pdftitle={\@title},
  pdfauthor={\@author},
  pdfsubject={Modelo de artigo científico com abnTeX2},
  pdfcreator={LaTeX with abnTeX2},
  pdfkeywords={abnt}{latex}{abntex}{abntex2}{atigo científico},
  colorlinks=false,
  pdfborder={0 0 0},
  bookmarksdepth=4}

\makeatother
\makeindex

\setlrmarginsandblock{4cm}{4cm}{*}
\setulmarginsandblock{4cm}{4cm}{*}
\checkandfixthelayout
\setlength{\parindent}{1.3cm}
\setlength{\parskip}{0.2cm}
\SingleSpacing

\begin{document}

\sloppy

\frenchspacing

\maketitle

\section*{Informações do autor}

{\parindent0pt

\textbf{Nome}: Vinicius Massuchetto

\textbf{Contato}: vmassuchetto@gmail.com

\textbf{Palavras-chave}: colaboratividade, software livre, sociedade da
informação

\textbf{Indicação do eixo}: Trabalho ontem e hoje

\textbf{Formação acadêmica}: Tecnólogo em Química Ambiental pela Universidade
Tecnológica Federal do Paraná, Bacharel em Ciência da Computação pela
Universidade Federal do Paraná, Mestrando no Programa de Pós-Graduação em
Tecnologia Nuclear do Instituto de Pesquisas Energéticas e Nucleares da
Universidade de São Paulo.

}

\section*{Resumo expandido}

O software da maneira como o conhecemos é um bem bastante peculiar na sociedade
atual, pois ao contrário de outros produtos que são comercializados há centenas
de anos seguindo o mesmo método de troca, o conjunto de informações contidas em
um programa não obedece às regras tradicionais do mercado. A comercialização do
software -- ou da informação, quando tratamos o software de uma maneira mais
ampla -- levou à criação de novos mecanismos regulatórios respaldados pelo
estado e suas instituições.

Ainda que de maneira informal, o software livre surge em meio à este contexto
para atender inicialmente as demandas acadêmicas, e foi aos poucos
integrando-se ao mercado e hoje mostra-se como uma alternativa viável às mais
diversas aplicações. Através de um engenhoso sistema de subversão dos
mecanismos regulatórios, o software livre possui a garantia de distribuição das
informações que o compõem através de um modelo livre de colaboração que
conhecemos por \emph{copyleft}, sendo assim capaz de modificar algumas relações
econômicas na área da tecnologia.

Além de conceituar sua temática, o presente trabalho busca subsidiar a escolha
dos parâmetros marxistas utilizados para analisar a configuração deste modelo
de produção, e apoia-se em autores que trouxeram a discussão marxista para além
da consolidação imperialista das décadas de 80 e 90, e que procuraram descrever
os processos de acumulação capitalista de uma sociedade que pauta fortemente a
sua economia no campo informacional.

A perspectiva deste contexto colocada por \cite{Meszaros2010} estabelece
que a transformação social prevista pelo marxismo deve ser capaz de ``enfrentar
as contingências sócio-históricas mutáveis e inevitáveis, reexaminando as
proposições básicas da teoria original e, se necessário, adaptando às novas
circunstâncias''. \cite{DyerWitheford1999}, por outro lado, vai ainda
além e rebate que ``se o marxismo é tido como obsoleto pela era da informação,
é somente pela luz do desenvolvimento `informacional' é que podemos ver a
completa importância de alguns temas dos textos de Marx''.

Dentre estes novos autores, uma interpretação controversa mas que possui
aspectos válidos para a discussão sobre colaboratividade é a visão autonomista
trazida por \cite{Negri1991}. O valor desta escola de pensamento não está
somente em buscar respostas às novas formas de luta na sociedade, mas também em
em enxergar as comunicações como uma área de profundas tensões na dinâmica
capitalista, já que a produtividade dos trabalhadores ali inseridos dependem de
uma complexa divisão do trabalho e de uma rede extremamente estruturada de
informações. Esta rede é capaz de abstrair e digitalizar operações que
tornam-se atividade intelectual produtiva, e por isto consiste em um sistema
maximizado de produção baseado em informação.

\cite{Soderberg2002} observa que neste sistema a explícita dependência da
economia da informação sobre a capacidade comunicativa e cooperativa da força
de trabalho retira parte do caráter controlador da produção dos agentes
capitalistas, e que um dos indicativos desta perda de controle são as
implementações livres de software e de protocolos de comunicação. Esta
interpretação é delicada quando observamos o ritmo de crescimento cada vez
maior das mega corporações do Vale do Silício às custas do software livre, mas
faz sentido quando vemos uma enorme rede de colaboração possibilitar o acesso à
tecnologia por setores que não o teriam se dependessem dos softwares
proprietários. Estas dinâmicas de troca e colaboração configuram-se em uma
contraposição ao modo de produção hegemônico através de um fenômeno em que um
processo de aprendizado coletivo torna-se uma entidade produtiva por si
própria.

A tese da auto-valorição descrita por \cite{Negri1991} prevê também que
os projetos contra-hegemônicos -- como sugere-se que o software livre o é --
não se qualificam necessariamente como um projeto social unificado e
revolucionário, mas somente denotam uma pluralidade de instâncias e
possibilidades que surgem através da insatisfação com a configuração do sistema
atual. Assim, estes projetos autônomos podem não só ser caracterizados como um
combate ao capitalismo, mas podem também ser parte dele em uma forma diferente
e alternativa de geração de valor.

Compreende-se as contradições a que estamos sujeitos quando desenvolvemos uma
análise desta natureza, mas entende-se também que as dinâmicas informacionais
precisam ser levadas em conta para que possamos abranger as relações sociais da
atualidade, e neste campo as perspectivas marxistas mostram-se como ferramentas
razoáveis para o esclarecimento do que se entende pela liberdade de
conhecimento, e quais as mudanças possíveis na sociedade a partir da
implementação de políticas que valorizem iniciativas relacionadas à cultura da
livre informação.

\section*{Referências}

\bibliography{../mfs.bib}

\end{document}
