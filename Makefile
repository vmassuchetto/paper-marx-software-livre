MAINFILE = mfs
FILES = $(MAINFILE).tex `ls pt-*.tex | grep -v "diff"`

.PHONY: all diff doc clean

all: pdf clear

pdf:
	pdflatex $(MAINFILE).tex
	bibtex $(MAINFILE).aux
	pdflatex $(MAINFILE).tex
	pdflatex $(MAINFILE).tex

diff:
	latexdiff-git $(hash) $(FILES)
	latexdiff-git $(hash) $(FILES)

html: pdf
	latex2html $(MAINFILE).tex \
		-split 0 \
		-no_navigation \
		-info "" \
		-address "" \
		-html_version 4.0,unicode

odt: html
	libreoffice --headless \
		--convert-to odt:"OpenDocument Text Flat XML" \
		$(MAINFILE)/index.html
	mv index.odt pt.odt

clear:
	rm -rf *diff* \
		img/*.4og \
		$(MAINFILE) \
		$(MAINFILE).aux \
		$(MAINFILE).log \
		$(MAINFILE).blg \
		$(MAINFILE).bbl \
		$(MAINFILE).toc \
		$(MAINFILE).out \
		$(MAINFILE).lof \
		$(MAINFILE).lot \
		$(MAINFILE).bib.bak

clean: clear
	rm -f *~ \
		$(MAINFILE).odt \
		$(MAINFILE).pdf \
		$(MAINFILE).doc
